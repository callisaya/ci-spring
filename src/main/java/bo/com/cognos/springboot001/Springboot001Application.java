package bo.com.cognos.springboot001;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class Springboot001Application extends SpringBootServletInitializer {

	Logger logger = LoggerFactory.getLogger(Springboot001Application.class);

	
    @Value("${app.name:No se encontro configuracion}")
	private String nombreAplicacion;
	
	public static void main(String[] args) {
		SpringApplication.run(Springboot001Application.class, args);
	}
	
	@RequestMapping(value = "/")
	public String hello() {
		logger.trace("TRACEEEE");
		logger.debug("DEBUGGGG");
		logger.info("INFOOOO");
		logger.warn("WARNNNN");
		logger.error("ERRORRRR");
	    return "Hola mundo desde Spring Boot: " + nombreAplicacion;
    }
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
	    return application.sources(Springboot001Application.class);
	}



}
